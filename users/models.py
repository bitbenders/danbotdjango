from django.contrib.auth.models import AbstractUser
from django.db import models
from django.db.models.signals import pre_save
from rest_framework.exceptions import ValidationError
from django.dispatch import receiver
import uuid



class CustomUser(AbstractUser):
    first_name = models.CharField(blank=True, null=True, max_length=255)
    last_name = models.CharField(blank=True, null=True, max_length=255)
    email = models.EmailField(blank=False, null=False, unique=True)
    discord_id = models.CharField(blank=True, null=True, max_length=256, unique=True)
    songs_upvoted = models.ManyToManyField('DanBotDjango.Song', related_name='user_songs_upvoted', blank=True)
    songs_remove_votes = models.ManyToManyField('DanBotDjango.Song', related_name='user_songs_remove_votes',  blank=True)
    songs_suggested = models.ManyToManyField('DanBotDjango.SongSuggestion', blank=True)

    def __str__(self):
        return self.email


# catch the save signal so that we can always make sure a user has a unique username
# TODO: we should move this to users/signals.py
@receiver(pre_save, sender=CustomUser)
def fill_username(sender, instance, *args, **kwargs):
    print(instance)
    if not instance.username:
        instance.username = str(uuid.uuid4())

