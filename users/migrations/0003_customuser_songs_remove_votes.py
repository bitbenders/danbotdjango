# Generated by Django 2.2.11 on 2020-04-08 01:08

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('DanBotDjango', '0006_auto_20200406_2325'),
        ('users', '0002_remove_customuser_songs_remove_votes'),
    ]

    operations = [
        migrations.AddField(
            model_name='customuser',
            name='songs_remove_votes',
            field=models.ManyToManyField(blank=True, related_name='user_songs_remove_votes', to='DanBotDjango.Song'),
        ),
    ]
