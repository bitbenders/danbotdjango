# users/admin.py
from django.contrib import admin
from django.contrib.auth import get_user_model
from django.contrib.auth.admin import UserAdmin
from .forms import CustomUserCreationForm, CustomUserChangeForm
from .models import CustomUser
from DanBotDjango.models import *


class CustomUserAdmin(UserAdmin):
    add_form = CustomUserCreationForm
    form = CustomUserChangeForm
    model = CustomUser
    list_display = ['id', 'first_name', 'last_name', 'email', 'is_staff', 'is_superuser']
    readonly_fields = ['username', ]
    filter_horizontal = ['songs_suggested', 'songs_upvoted']

    # This might be how we add to the form?
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            # these match up with the fields on the model
            'fields': ('username', 'email', 'password1', 'password2', ),
        }),
    )

    fieldsets = (
        ('User Information', {
            'classes': ('wide',),
            # these match up with the fields on the model
            'fields': ('username', 'email', 'discord_id', 'songs_upvoted', 'songs_suggested', 'songs_remove_votes'),
        }),
        ('Password', {
            'classes': ('wide',),
            'fields': ('password',),
        }),
        ('Roles', {
            'classes': ('wide',),
            'fields': ('is_staff', 'is_active', 'is_superuser'),
        }),
        ('Activity', {
            'classes': ('wide',),
            'fields': ('date_joined', 'last_login',),
        })
    )

    # def get_orders(self, obj):
    #     # cleanly format the question answers
    #     returned_string = ""
    #     for i in obj.orders.all():
    #         returned_string += (str(i.question) + " (" + str(obj) + ")\n")
    #
    #     return returned_string
    #
    # get_orders.short_description = "Question Answers"


admin.site.register(CustomUser, CustomUserAdmin)