from allauth.account.adapter import DefaultAccountAdapter
from rest_framework.exceptions import APIException


class CustomUserAccountAdapter(DefaultAccountAdapter):

    def save_user(self, request, user, form, commit=True):
        """
        Saves a new `User` instance using information provided in the
        signup form.
        """
        from allauth.account.utils import user_field, user_username, user_email

        user = super().save_user(request, user, form, False)

        user_field(user, 'first_name', request.data.get('first_name', ''))
        user_field(user, 'last_name', request.data.get('last_name', ''))
        self.populate_username(request, user)

        # TODO: do we want to change this? -1 is an invalid account type so it will error
        user.set_password(request.data['password'])
        user.save()
        return user
