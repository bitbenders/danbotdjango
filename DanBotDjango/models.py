from django.contrib.auth import get_user_model
from django.contrib.postgres.fields import ArrayField
from django.db import models
from rest_framework.exceptions import ValidationError
import datetime
import uuid
from django.db.models.signals import m2m_changed, post_save, pre_save

User = get_user_model()


class Song(models.Model):
    CHILLSTEP = 0
    HEAVY_METAL = 1
    ROCK_ROLL = 2

    GENRES = (
        (CHILLSTEP, 'Chill Step'),
        (HEAVY_METAL, 'Heavy Metal'),
        (ROCK_ROLL, 'Rock N Roll'),
    )

    name = models.CharField(blank=False, null=False, max_length=128)
    up_votes = models.IntegerField(blank=False, null=False, default=0)
    genre = models.PositiveSmallIntegerField(blank=False, null=False, choices=GENRES)
    song_file = models.FileField(blank=False, null=False)
    remove_votes = models.IntegerField(blank=True, null=True, default=0)
    date_added = models.DateTimeField(auto_now_add=True, blank=False, null=False)

    def __str__(self):
        return self.name


class SongSuggestion(models.Model):
    name = models.CharField(blank=False, null=False, max_length=128)
    up_votes = models.IntegerField(blank=False, null=False, default=1)
    date_added = models.DateTimeField(auto_now_add=True, blank=False, null=False)

    def __str__(self):
        return self.name


'''example model
class Bin(models.Model):
    SMALL = 0
    MEDIUM = 1
    LARGE = 2

    BIN_TYPES = (
        (SMALL, '27 Gallon Bin'),
        (MEDIUM, '38 Gallon Bin'),
        (LARGE, '55 Gallon Bin'),
    )

    EMPTY_BIN_WITH_CUSTOMER = 0
    IN_TRANSIT_TO_WAREHOUSE_FOR_STORAGE = 1
    IN_STORAGE = 2
    IN_TRANSIT_TO_CUSTOMER = 3
    READY_FOR_USE = 4
    DELIVERED_TO_CUSTOMER = 5
    IN_TRANSIT_TO_WAREHOUSE_EMPTY = 6
    WAITING_FOR_PICKUP = 7

    DELIVERY_STATUS = (
        (EMPTY_BIN_WITH_CUSTOMER, 'Bin Dropped off to Customer - Empty'),
        (IN_TRANSIT_TO_WAREHOUSE_FOR_STORAGE, 'Picked up From Customer'),
        (IN_STORAGE, 'Dropped off at Warehouse (In storage)'),
        (IN_TRANSIT_TO_CUSTOMER, 'Picked Up from Warehouse (In transit to customer)'),
        (READY_FOR_USE, 'Ready for use'),
        (DELIVERED_TO_CUSTOMER, 'Delivered to Customer'),
        (IN_TRANSIT_TO_WAREHOUSE_EMPTY, 'Empty Bins picked up from customer'),
        (WAITING_FOR_PICKUP, 'Waiting for pickup')
    )

    bin_type = models.PositiveSmallIntegerField(choices=BIN_TYPES, blank=False, null=False)
    bin_id = models.UUIDField(blank=False, null=False, default=uuid.uuid4, editable=False)
    rental = models.NullBooleanField(default=None, help_text='This will be set automatically when attached to an order')
    status = models.PositiveSmallIntegerField(choices=DELIVERY_STATUS, default=4, blank=False, null=False)
    images = ArrayField(models.ImageField(blank=True), blank=True)
    warehouse_location = models.CharField(max_length=500, blank=True, null=True)

    def __str__(self):
        return str(self.bin_id) + " (" + str(self.BIN_TYPES[self.bin_type][1]) + ")" 
'''