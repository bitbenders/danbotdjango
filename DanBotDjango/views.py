from django.contrib.auth import get_user_model
from django.shortcuts import redirect
from rest_framework import viewsets
from rest_framework.decorators import api_view, permission_classes, action
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework.response import Response
from rest_framework.authtoken.models import Token
from rest_framework.authtoken.views import ObtainAuthToken
from rest_framework.views import APIView


from DanBotDjango.models import *
from .serializers import *
from .paginators import StandardPagination
from .utils import strip_non_model_fields
from users.models import CustomUser
import uuid

# IMPORTANT BECAUSE WE USE A CUSTOM USER
User = get_user_model()


def redirect_to_api(request):
    # redirect to api endpoint
    return redirect('/api')


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all().order_by('id').reverse()
    serializer_class = UserSerializer
    pagination_class = StandardPagination

    def get_permissions(self):
        if self.action == 'create':
            permission_classes = [AllowAny]
        else:
            permission_classes = [IsAuthenticated]

        return [permission() for permission in permission_classes]

    def create(self, request, *args, **kwargs):
        print('register user')
        print(request.data)

        # this will throw an error if it's invalid, so no need for an if()
        self.get_serializer(data=request.data).is_valid(raise_exception=True)
        user_data = strip_non_model_fields(request.data, User)

        print(user_data)

        user = User.objects.create_user(
            # use a uuid here because it's required and we don't actually use username
            username=str(uuid.uuid4()),
            **user_data
        )

        print(user)
        print(user.values())

        token = Token.objects.create(user=user)

        return Response({'token': token.key, **user.values()}, status=201)


class DiscordUserViewSet(viewsets.ViewSet):
    queryset = User.objects.filter(username='').order_by('id').reverse()
    serializer_class = UserSerializer
    pagination_class = StandardPagination
    permission_classes = (AllowAny,)

    def create(self, request):
        print('create discord user')

        serializer = DiscordUserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        print(serializer.data)

        return Response(serializer.data, status=201)


class SongViewSet(viewsets.ModelViewSet):
    queryset = Song.objects.all().order_by('id').reverse()
    serializer_class = SongSerializer
    pagination_class = StandardPagination
    # TODO remove this and put IsAuthenticated
    permission_classes = (AllowAny,)

    def list(self, request, *args, **kwargs):
        name = request.query_params.get('name')
        if not name:
            return Response(SongSerializer(self.get_queryset(), many=True).data, status=200)

        matchingSongs = Song.objects.filter(name__iexact=name)
        return Response(SongSerializer(matchingSongs, many=True).data, status=200)

    @action(detail=False, methods=['get'], name='genres', url_path='genres/(?P<genre_id>[^/.]+)', url_name='genres')
    def genre(self, request, genre_id=None, *args, **kwargs):
        if genre_id is None:
            return Response(status=404)

        songs = Song.objects.filter(genre=genre_id).order_by('up_votes').reverse()
        return Response(SongSerializer(songs, many=True).data, status=200)

    @action(detail=False, methods=['post'], name='up-vote', url_path='up-vote', url_name='up-vote')
    def upvote(self, request, *args, **kwargs):
        name = request.data.get('name')
        discord_id = request.data.get('discord_id')

        if not name:
            return Response('no name was given', status=400)
        if not discord_id:
            return Response('no valid discord id', status=400)

        user = User.objects.filter(discord_id=discord_id)
        if not user.exists():
            return Response('discord user is not valid', status=404)
        if len(user) > 1:
            return Response('multiple users with same ID returned', status=400)


        song = Song.objects.filter(name__iexact=name)
        if not song.exists():
            return Response(status=404)

        if len(song) > 1:
            return Response(status=400)

        song = song[0]
        user = user[0]

        users_upvoted = User.objects.filter(songs_upvoted__in=[song])
        if user in users_upvoted:
            return Response('you have already upvoted song', status=400)

        song.up_votes += 1
        song.save()

        user.songs_upvoted.add(song)
        user.save()


        return Response(status=200)

    @action(detail=False, methods=['post'], name='remove-vote', url_path='remove-vote', url_name='remove-vote')
    def removevote(self, request, *args, **kwargs):
        name = request.data.get('name')
        discord_id = request.data.get('discord_id')

        if not name:
            return Response('no name was given', status=400)
        if not discord_id:
            return Response('no valid discord id', status=400)

        user = User.objects.filter(discord_id=discord_id)
        if not user.exists():
            return Response('discord user is not valid', status=404)
        if len(user) > 1:
            return Response('multiple users with same ID returned', status=400)


        song = Song.objects.filter(name__iexact=name)
        if not song.exists():
            return Response(status=404)

        if len(song) > 1:
            return Response(status=400)

        song = song[0]
        user = user[0]

        users_upvoted = User.objects.filter(songs_upvoted__in=[song])
        if user in users_upvoted:
            return Response('you have already upvoted song', status=400)

        song.remove_votes += 1
        song.save()

        user.songs_upvoted.add(song)
        user.save()


        return Response(status=200)





class SongSuggestionViewSet(viewsets.ModelViewSet):
    queryset = SongSuggestion.objects.all().order_by('id').reverse()
    serializer_class = SongSuggestionSerializer
    pagination_class = StandardPagination
    # TODO remove this and put IsAuthenticated
    permission_classes = (AllowAny,)

    def list(self, request, *args, **kwargs):
        name = request.query_params.get('name')
        if not name:
            return Response(SongSuggestionSerializer(self.get_queryset(), many=True).data, status=200)

        matchingSongs = SongSuggestion.objects.filter(name__iexact=name)
        return Response(SongSuggestionSerializer(matchingSongs, many=True).data, status=200)

    @action(detail=False, methods=['post'], name='suggest', url_path='suggest', url_name='suggest')
    def suggest(self, request, *args, **kwargs):
        name = request.data.get('name')
        discord_id = request.data.get('discord_id')

        if not name:
            return Response(status=400)
        if not discord_id:
            return Response(status=400)

        user = User.objects.filter(discord_id=discord_id)
        if not user.exists():
            return Response(status=404)
        if len(user) > 1:
            return Response(status=400)


        song = SongSuggestion.objects.filter(name__iexact=name)
        if not song.exists():
            song = SongSuggestion.objects.create(name=song.name)

        if len(song) > 1:
            return Response(status=400)

        song = song[0]
        user = user[0]

        users_upvoted = User.objects.filter(songs_suggested__in=[song])
        if user in users_upvoted:
            return Response('you have already suggested song', status=400)

        song.up_votes += 1
        song.save()

        user.songs_suggested.add(song)
        user.save()


        return Response(status=200)

