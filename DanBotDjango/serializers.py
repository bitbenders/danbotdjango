from rest_framework import serializers
from django.contrib.auth import get_user_model, authenticate
from .models import *
from rest_framework.exceptions import ValidationError
from django.shortcuts import get_object_or_404
import datetime


User = get_user_model()


class LoginSerializer(serializers.Serializer):
    email = serializers.CharField()
    password = serializers.CharField()

    def validate(self, data):
        email = data.get('email')
        password = data.get('password')

        print(email)
        print(password)

        if email is None:
            raise ValidationError({"email": ['Please fill out all fields']})
        if password is None:
            raise ValidationError({"password": ['Please fill out all fields']})

        user = authenticate(email=email, password=password)
        print(user)

        if user:
            return user

        else:
            raise ValidationError({'non_field_errors': ['Invalid Login']})


class UserSerializer(serializers.ModelSerializer):

    class Meta:
        model = User
        # these fields also define what can be posted, for example if we get rid of "password" we can't post a password, which is bad
        fields = "__all__"
        extra_kwargs = {
            'password': {'write_only': True},
            'username': {'required': False},
            'email': {'required': True},
        }


class DiscordUserSerializer(serializers.Serializer):
    discord_id = serializers.CharField(required=True)

    def validate(self, attrs):
        if not User.objects.filter(discord_id=attrs['discord_id']).exists():
            return attrs

        raise ValidationError({'discord_id': 'A user with this Id already exists'})

    def create(self, validated_data):
        user = User.objects.create_user(
            username=str(uuid.uuid4()),
            email=str(uuid.uuid4()),
            discord_id=validated_data['discord_id']
        )

        return user


class SongSerializer(serializers.ModelSerializer):
    date_added = serializers.SerializerMethodField('format_date_added')

    def format_date_added(self, song):
        return song.date_added.strftime('%m/%d/%Y')

    class Meta:
        model = Song
        fields = "__all__"


class SongSuggestionSerializer(serializers.ModelSerializer):
    date_added = serializers.SerializerMethodField('format_date_added')

    def format_date_added(self, song):
        return song.date_added.strftime('%m/%d/%Y')

    class Meta:
        model = SongSuggestion
        fields = "__all__"
        extra_kwargs = {
            'up_votes': {'required': False},
        }
