from django.contrib import admin
from django.urls import reverse
from django.utils.safestring import mark_safe

from .models import *
from allauth.socialaccount.models import SocialAccount, SocialToken, SocialApp
from django.contrib.sites.models import Site


User = get_user_model()


class SongAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'get_genre_name', 'up_votes', 'remove_votes')
    search_fields = ('name',)

    def get_genre_name(self, obj):
        return Song.GENRES[obj.genre][1]
    get_genre_name.short_description = "Genre"


class SongSuggestionAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'up_votes',)
    search_fields = ('name',)



admin.site.register(Song, SongAdmin)
admin.site.register(SongSuggestion, SongSuggestionAdmin, )

'''
class BinAdmin(admin.ModelAdmin):
    list_display = ('bin_id', 'bin_type', 'get_bin_order', 'status',)
    list_filter = ('bin_type', 'status')
    readonly_fields = ['bin_id', 'rental']

    def get_bin_order(self, obj):
        order = Order.objects.filter(bins__in=[obj.id])
        if order.exists() and order.count() == 1:
            result = order.first()
            ct = result._meta
            url = reverse('admin:%s_%s_change' % (ct.app_label, ct.model_name), args=(result.id,))
            return mark_safe('<a href="%s"><strong>%s</strong></a>' % (url, result.uuid))
        elif order.exists():
            return 'Multiple orders found'
        else:
            return 'None'

    get_bin_order.allow_tags = True
    get_bin_order.short_description = "Order"
'''


''' example requirement for models, (binadmin is optional)
admin.site.register(Bin, BinAdmin)
'''
